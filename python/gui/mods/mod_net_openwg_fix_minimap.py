
import json
import os

import Event

from debug_utils import LOG_CURRENT_EXCEPTION
from frameworks.wulf import WindowLayer
from gui.Scaleform.framework import g_entitiesFactories, ViewSettings, ScopeTemplates
from gui.Scaleform.framework.entities.BaseDAAPIComponent import BaseDAAPIComponent
from gui.Scaleform.framework.entities.DisposableEntity import EntityState
from gui.Scaleform.framework.entities.View import View
from gui.Scaleform.framework.managers.loaders import SFViewLoadParams
from gui.shared import events, EVENT_BUS_SCOPE, g_eventBus
from gui.shared.personality import ServicesLocator
from skeletons.gui.app_loader import GuiGlobalSpaceID

def byteify(data):
	"""Encodes data with UTF-8
	:param data: Data to encode"""
	result = data
	if isinstance(data, dict):
		result = {byteify(key): byteify(value) for key, value in data.iteritems()}
	elif isinstance(data, (list, tuple, set)):
		result = [byteify(element) for element in data]
	elif isinstance(data, unicode):
		result = data.encode('utf-8')
	return result

SETTINGS_FILE = 'mods/configs/net.openwg/minimap_fix.json'
FLASH_COMPONENT_LINKAGE = 'OpenwgFixMinimapUI'
FLASH_INJECTOR_LINKAGE = 'OpenwgFixMinimapInjector'
FLASH_FILENAME = 'openwg-fix-minimap.swf'

class MainController:

	def __init__(self):
		self.handleVehicleAoI = Event.SafeEvent()
		self.handleShowExtended = Event.SafeEvent()
		self.readSettings()

	def onGUISpaceEntered(self, spaceID):
		if spaceID != GuiGlobalSpaceID.BATTLE:
			return
		app = ServicesLocator.appLoader.getDefBattleApp()
		app.loadView(SFViewLoadParams(FLASH_INJECTOR_LINKAGE))

	def handleShowExtendedInfo(self, event):
		self.handleShowExtended(event.ctx['isDown'])

	def readSettings(self):
		self.settings = {
			'defaultMode': 'adaptive',
			'extendedMode': 'adaptive',
		}
		loaded = False
		settings_dir = os.path.dirname(SETTINGS_FILE)
		try:
			if not os.path.isdir(settings_dir):
				os.makedirs(settings_dir)
			if os.path.isfile(SETTINGS_FILE):
				with open(SETTINGS_FILE, 'rb') as fh:
					self.settings.update(byteify(json.load(fh)))
					loaded = True
		except IOError:
			pass
		except:
			LOG_CURRENT_EXCEPTION()

		if not loaded:
			self.saveSettings()

	def saveSettings(self):
		try:
			with open(SETTINGS_FILE, 'wb') as fh:
				json.dump(self.settings, fh, ensure_ascii=False, indent=4, 
									separators=(',', ': '), sort_keys=True)
		except:
			LOG_CURRENT_EXCEPTION()

g_controller = MainController()

class OpenwgFixMinimapView(BaseDAAPIComponent):

	def _populate(self):
		super(OpenwgFixMinimapView, self)._populate()
		self._setSettings()
		g_controller.handleVehicleAoI += self._handleVehicleAoI
		g_controller.handleShowExtended += self._handleShowExtended

	def _dispose(self):
		g_controller.handleVehicleAoI -= self._handleVehicleAoI
		g_controller.handleShowExtended -= self._handleShowExtended
		super(OpenwgFixMinimapView, self)._dispose()

	def _setSettings(self):
		if self._isDAAPIInited():
			self.flashObject.as_setSettings(g_controller.settings)

	def _handleVehicleAoI(self):
		if self._isDAAPIInited():
			self.flashObject.as_handleVehicleAoI()

	def _handleShowExtended(self, isDown):
		if self._isDAAPIInited():
			self.flashObject.as_handleShowExtended(isDown)

	def destroy(self):
		if self.getState() != EntityState.CREATED:
			return
		super(OpenwgFixMinimapView, self).destroy()

g_entitiesFactories.addSettings(ViewSettings(FLASH_INJECTOR_LINKAGE, View, FLASH_FILENAME, WindowLayer.WINDOW, None, ScopeTemplates.GLOBAL_SCOPE))
g_entitiesFactories.addSettings(ViewSettings(FLASH_COMPONENT_LINKAGE, OpenwgFixMinimapView, None, WindowLayer.UNDEFINED, None, ScopeTemplates.DEFAULT_SCOPE))

def init():
	ServicesLocator.appLoader.onGUISpaceEntered += g_controller.onGUISpaceEntered
	g_eventBus.addListener(events.GameEvent.SHOW_EXTENDED_INFO, g_controller.handleShowExtendedInfo, scope=EVENT_BUS_SCOPE.BATTLE)

def fini():
	ServicesLocator.appLoader.onGUISpaceEntered -= g_controller.onGUISpaceEntered
	g_eventBus.removeListener(events.GameEvent.SHOW_EXTENDED_INFO, g_controller.handleShowExtendedInfo, scope=EVENT_BUS_SCOPE.BATTLE)
