﻿package
{
	import mods.common.AbstractComponentInjector;
	import me.poliroid.battle.OpenwgFixMinimap;
	
	public class OpenwgFixMinimapInjector extends AbstractComponentInjector
	{
		override protected function onPopulate() : void
		{
			autoDestroy = false;
			componentName = "OpenwgFixMinimapUI";
			componentUI = OpenwgFixMinimap;
			super.onPopulate();
		}
	}
}