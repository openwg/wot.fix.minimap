﻿package me.poliroid.battle
{

	import flash.text.TextField;

	import net.wg.gui.battle.views.minimap.components.entries.vehicle.VehicleMinimapEntry;
	import net.wg.gui.battle.views.minimap.MinimapEntryController;

	import mods.common.BattleDisplayable;

	public class OpenwgFixMinimap extends BattleDisplayable
	{

		private var _settings:Object;
		private var _show_extended:Boolean = false;

		public function as_setSettings(settings:Object) : void
		{
			_settings = settings;
			var entry:VehicleMinimapEntry;
			// handle visible tanks
			for each(entry in MinimapEntryController.instance['_vehicleEntries'])
				if(entry['_labelHelper'] as AdvancedLabelHelper == null)
					_swap_label_helper(entry);
			// handle invisible tanks
			for each(entry in MinimapEntryController.instance['_vehicleLabelsEntries'])
				if(entry['_labelHelper'] as AdvancedLabelHelper == null)
					_swap_label_helper(entry);
		}

		public function as_handleShowExtended(isDown:Boolean) : void
		{
			_show_extended = isDown;
			var entry:VehicleMinimapEntry;
			// handle visible tanks
			for each(entry in MinimapEntryController.instance['_vehicleEntries'])
				entry['_labelHelper'].show_extended = _show_extended;
			// handle invisible tanks
			for each(entry in MinimapEntryController.instance['_vehicleLabelsEntries'])
				entry['_labelHelper'].show_extended = _show_extended;
		}

		private function _swap_label_helper(entry:VehicleMinimapEntry) : void
		{
			// create own helper
			var new_helper:AdvancedLabelHelper = new AdvancedLabelHelper(entry, _settings);
			new_helper.show_extended = _show_extended;
			// dispose and swap orginal helper
			entry['_labelHelper'].dispose();
			entry['_labelHelper'] = new_helper;
		}
	}
}
