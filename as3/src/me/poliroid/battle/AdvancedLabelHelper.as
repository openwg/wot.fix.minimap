﻿package me.poliroid.battle
{

	import flash.text.TextField;

	import net.wg.gui.battle.views.minimap.components.entries.vehicle.MinimapEntryLabelHelper;
	import net.wg.gui.battle.views.minimap.components.entries.vehicle.VehicleMinimapEntry;

	public dynamic class AdvancedLabelHelper extends MinimapEntryLabelHelper
	{

		private var _show_extended:Boolean = false;

		private var _settings:Object = null;

		public function AdvancedLabelHelper(vehicleEntry:VehicleMinimapEntry, settings:Object) : void
		{
			// store our settings
			_settings = settings;
			// restore original TF position
			var labelHelper = vehicleEntry['_labelHelper'];
			labelHelper._currentTF.x = labelHelper._initialOffsetX;
			labelHelper._currentTF.y = labelHelper._initialOffsetY;
			// construct original
			super(vehicleEntry, labelHelper._currentTF);

			// restore size index
			var base:* = this;
			base._sizeIndex = labelHelper._sizeIndex;

			// restore running state
			if (labelHelper._timer.running)
				base._timer.start();
		}

		public function get show_extended() : Boolean
		{
			return _show_extended;
		}

		public function set show_extended(value:Boolean) : void
		{
			_show_extended = value;
			forceUpdate();
		}

		public function set settings(value:Object) : void
		{
			_settings = value;
			forceUpdate();
		}

		private function update(forced:Boolean = false) : void
		{
			switch(show_extended ? _settings.extendedMode : _settings.defaultMode)
			{
				case 'adaptive':
					update_adaptive(forced);
					break;
				case 'left':
					update_left(forced);
					break;
				case 'right':
					update_right(forced);
					break;
				default:
					break;
			}
		}

		private function update_adaptive(forced:Boolean) : void
		{
			// just call original MinimapEntryLabelHelper method
			super['update'](forced);
		}

		private function update_right(forced:Boolean) : void
		{
			var base:* = this;
			var entryRef:VehicleMinimapEntry = base._entryRef;

			if(!entryRef.isVehicleLabelVisible)
				return;

			if(forced || base.__prevEntityX != entryRef.x || base.__prevEntityY != entryRef.y)
			{
				base.__prevEntityX = entryRef.x;
				base.__prevEntityY = entryRef.y;
				base._currentTF.x = entryRef.hpCircle.visible ? base._hpOffsetX : base._initialOffsetX;
				base._currentTF.y = entryRef.hpCircle.visible ? base._hpOffsetY : base._initialOffsetY;
			}
		}

		private function update_left(forced:Boolean) : void
		{
			var base:* = this;
			var entryRef:VehicleMinimapEntry = base._entryRef;

			if(!entryRef.isVehicleLabelVisible)
				return;

			if(forced || base.__prevEntityX != entryRef.x || base.__prevEntityY != entryRef.y)
			{
				base.__prevEntityX = entryRef.x;
				base.__prevEntityY = entryRef.y;
				var posX:int = entryRef.hpCircle.visible ? base._hpOffsetX : base._initialOffsetX;
				base._currentTF.x = -(base._currentTF.width + posX);
				base._currentTF.y = entryRef.hpCircle.visible ? base._hpOffsetY : base._initialOffsetY;
			}
		}
	}
}
